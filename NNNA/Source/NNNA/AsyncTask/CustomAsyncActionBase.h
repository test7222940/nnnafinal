// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "CustomAsyncActionBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFinished);

UCLASS(Blueprintable)
class NNNA_API UCustomAsyncActionBase : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
	
public:

	UFUNCTION(Blueprintcallable, meta = (vlueprintinternalUseOnly = true, WorldContext = "World"))
	static UCustomAsyncActionBase* CustomWaitDelay(UObject* World, float DelayTime, UCustomAsyncActionBase*&ThisClass);

	UFUNCTION(Blueprintcallable)
		void Destroy();

	UPROPERTY(BlueprintAssignable)
	FOnFinished OnDelayFinished;

	virtual void Activate() override;

	UPROPERTY(Blueprintreadwrite)
	FTimerHandle TimerHandle;

private:
	UPROPERTY()
	UObject* m_World;

	void StartDelay();

	float m_DelayTime;
};
