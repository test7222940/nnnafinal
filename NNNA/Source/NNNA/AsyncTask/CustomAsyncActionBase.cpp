// Fill out your copyright notice in the Description page of Project Settings.

#include "CustomAsyncActionBase.h"

UCustomAsyncActionBase* UCustomAsyncActionBase::CustomWaitDelay(UObject* World, float DelayTime, UCustomAsyncActionBase*& ThisClass)
{
	UCustomAsyncActionBase* MyClass = NewObject< UCustomAsyncActionBase>();
	MyClass->m_World = World;
	MyClass->m_DelayTime = DelayTime;
	ThisClass = MyClass;
	return MyClass;
}

void UCustomAsyncActionBase::Destroy()
{
	Super::SetReadyToDestroy();
}

void UCustomAsyncActionBase::Activate()
{
	Super::Activate();

	UWorld* World = GEngine->GetWorldFromContextObjectChecked(m_World);

	World->GetTimerManager().SetTimer(TimerHandle, this, &UCustomAsyncActionBase::StartDelay,m_DelayTime);

}

void UCustomAsyncActionBase::StartDelay()
{
	OnDelayFinished.Broadcast();
}
