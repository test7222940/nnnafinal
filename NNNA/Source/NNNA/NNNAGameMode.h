// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NNNAGameMode.generated.h"

UCLASS(minimalapi)
class ANNNAGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANNNAGameMode();
};



