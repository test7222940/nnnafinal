// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityBase.h"

UWorld* UAbilityBase::GetWorld() const
{
	if (HasAnyFlags(RF_ClassDefaultObject)) {
		return nullptr;
	}

	return GetOuter()->GetWorld();
}

void UAbilityBase::PostInitProperties()
{
	Super::PostInitProperties();
	if (GetWorld()) {
		BeginPlay();
	}
}

void UAbilityBase::Destroy()
{
	K2_BeginDestroy();
	this->ConditionalBeginDestroy();
}