// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AbilityBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class NNNA_API UAbilityBase : public UObject
{
	GENERATED_BODY()

public:

	virtual UWorld* GetWorld() const override;
	virtual void PostInitProperties() override;

	UFUNCTION(BlueprintImplementableEvent)
	void BeginPlay();

	UFUNCTION(BlueprintCallable)
	void Destroy();

	UFUNCTION(BlueprintImplementableEvent)
	void K2_BeginDestroy();

};
